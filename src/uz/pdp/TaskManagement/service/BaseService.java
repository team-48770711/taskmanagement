package uz.pdp.TaskManagement.service;

import uz.pdp.TaskManagement.domain.DTO.Response;

public interface BaseService<CD> {
    Response add(CD cd);
}
