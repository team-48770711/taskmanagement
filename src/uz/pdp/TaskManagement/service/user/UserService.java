package uz.pdp.TaskManagement.service.user;

import uz.pdp.TaskManagement.domain.DTO.SignUpDTO;
import uz.pdp.TaskManagement.domain.model.User;
import uz.pdp.TaskManagement.service.BaseService;

public interface UserService extends BaseService<SignUpDTO> {

}
