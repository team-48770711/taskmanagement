package uz.pdp.TaskManagement.service.task;

import uz.pdp.TaskManagement.domain.DTO.Response;
import uz.pdp.TaskManagement.domain.model.Task;
import uz.pdp.TaskManagement.repository.task.TaskRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class TaskServiceImpl implements TaskService {
    // private static TaskRepositoryImpl instance = TaskRepositoryImpl.getInstance();
    public static TaskRepositoryImpl taskRepository = TaskRepositoryImpl.getInstance();


    public TaskServiceImpl() {
    }

    @Override
    public Response add(Task task) {
        return null;
    }

    @Override
    public ArrayList<Task> getTasksByUserId(UUID userId) {
        ArrayList<Task> taskList = taskRepository.getAll();
        ArrayList<Task> userTasks = new ArrayList<>();

        for (Task task : taskList) {
            if (Objects.equals(task.getAssignedById(), userId)) {
                userTasks.add(task);
            }
        }
        return userTasks;
    }

    @Override
    public void printTasksByUserId(UUID userId) {
        ArrayList<Task> userTasks = this.getTasksByUserId(userId);

        int i = 1;
        for (Task task : userTasks) {
            System.out.println(i++ + " " + task.getTitle() + " " + task.getDescription() + " " + task.getUserStatus());
        }
    }
}
