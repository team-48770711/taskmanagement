package uz.pdp.TaskManagement.service.task;

import uz.pdp.TaskManagement.domain.model.Task;
import uz.pdp.TaskManagement.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface TaskService extends BaseService<Task> {
    ArrayList<Task> getTasksByUserId(UUID userId);

    void printTasksByUserId(UUID userId);
}
