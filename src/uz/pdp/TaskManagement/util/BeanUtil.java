package uz.pdp.TaskManagement.util;

import uz.pdp.TaskManagement.repository.task.TaskRepository;
import uz.pdp.TaskManagement.repository.task.TaskRepositoryImpl;
import uz.pdp.TaskManagement.repository.user.UserRepository;
import uz.pdp.TaskManagement.repository.user.UserRepositoryImpl;
import uz.pdp.TaskManagement.service.task.TaskService;
import uz.pdp.TaskManagement.service.task.TaskServiceImpl;
import uz.pdp.TaskManagement.service.user.UserService;
import uz.pdp.TaskManagement.service.user.UserServiceImpl;

import java.util.Scanner;

public interface BeanUtil {
    Scanner scanNum = new Scanner(System.in);
    Scanner scanStr = new Scanner(System.in);
    TaskRepository taskRepository = TaskRepositoryImpl.getInstance();
    UserRepository userRepository = UserRepositoryImpl.getInstance();
    TaskService taskService = new TaskServiceImpl();
    UserService userService = new UserServiceImpl();
}
