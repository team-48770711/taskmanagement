package uz.pdp.TaskManagement.domain.DTO;

public class Response<T> {
    private String message;
    private T data;
    private int status;

    public Response(String message) {
        this.message = message;
    }

    public Response(String message, T data, int status) {
        this.message = message;
        this.data = data;
        this.status = status;
    }

    public Response(String message, int status) {
        this.message = message;
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return message;
    }
}
