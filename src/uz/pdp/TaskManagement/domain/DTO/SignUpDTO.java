package uz.pdp.TaskManagement.domain.DTO;

public record SignUpDTO(String name, String phoneNumber, String password) {
}
