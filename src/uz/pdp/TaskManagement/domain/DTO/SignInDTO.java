package uz.pdp.TaskManagement.domain.DTO;

public record SignInDTO(String phoneNumber, String password) {
}
