package uz.pdp.TaskManagement.domain.model;

import java.util.UUID;

public abstract class BaseModel {
    {
        id= UUID.randomUUID();
    }
    private UUID id;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
