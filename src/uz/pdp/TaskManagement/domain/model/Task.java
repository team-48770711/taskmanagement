package uz.pdp.TaskManagement.domain.model;

import uz.pdp.TaskManagement.service.task.TaskService;

import java.util.UUID;

public class Task extends BaseModel{
    private String title;
    private String description;
    private UUID AssignedById;
    private UserStatus userStatus;
    private TaskStatus taskStatus;

    public Task(String title, String description, UUID assignedById, UserStatus userStatus, TaskStatus taskStatus) {
        this.title = title;
        this.description = description;
        AssignedById = assignedById;
        this.userStatus = userStatus;
        this.taskStatus = taskStatus;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getAssignedById() {
        return AssignedById;
    }

    public void setAssignedById(UUID assignedById) {
        AssignedById = assignedById;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }
}
