package uz.pdp.TaskManagement.domain.model;

public enum UserStatus {
    MANAGER,
    BACKEND_L,
    FRONTEND_L,
    BACKEND_DEV,
    FRONTEND_DEV,
    TESTER,
    USER
}
