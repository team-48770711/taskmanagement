package uz.pdp.TaskManagement.domain.model;

public class User extends BaseModel{
    private String name;
    private String phoneNumber;
    private String password;
    private UserStatus userStatus;

    public User(UserBuilder userBuilder) {
        this.name = userBuilder.name;
        this.phoneNumber = userBuilder.phoneNumber;
        this.password = userBuilder.password;
        this.userStatus = userBuilder.userStatus;
    }

    private static class UserBuilder {
        private String name;
        private String phoneNumber;
        private String password;
        private UserStatus userStatus;

        public User build() {
            return new User(this);
        }

        public UserBuilder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public UserBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder setUserStatus(UserStatus userStatus) {
            this.userStatus = userStatus;
            return this;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }
}
