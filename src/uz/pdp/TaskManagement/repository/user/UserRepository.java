package uz.pdp.TaskManagement.repository.user;

import uz.pdp.TaskManagement.domain.model.User;
import uz.pdp.TaskManagement.repository.BaseRepository;

public interface UserRepository extends BaseRepository<User> {
}
