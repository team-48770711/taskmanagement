package uz.pdp.TaskManagement.repository.user;

import uz.pdp.TaskManagement.domain.DTO.Response;
import uz.pdp.TaskManagement.domain.model.User;

import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository{
    private final ArrayList<User> USERS = new ArrayList<>();

    private static final UserRepositoryImpl instance = new UserRepositoryImpl();

    private UserRepositoryImpl() {
    }

    public static UserRepositoryImpl getInstance() {
        return instance;
    }
    @Override
    public Response save(User user) {
        USERS.add(user);
        return new Response("Success",200);
    }

    @Override
    public User getById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<User> getAll() {
        return USERS;
    }

    @Override
    public Response remove(User user) {
        return null;
    }

    @Override
    public Response removeById(UUID id) {
        return null;
    }
}
