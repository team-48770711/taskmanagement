package uz.pdp.TaskManagement.repository;

import uz.pdp.TaskManagement.domain.DTO.Response;

import java.util.ArrayList;
import java.util.UUID;

public interface BaseRepository<T> {
    Response save(T t);

    T getById(UUID id);

    ArrayList<T> getAll();

    Response remove(T t);

    Response removeById(UUID id);
}
