package uz.pdp.TaskManagement.repository.task;

import uz.pdp.TaskManagement.domain.model.Task;
import uz.pdp.TaskManagement.repository.BaseRepository;

public interface TaskRepository extends BaseRepository<Task> {
}
