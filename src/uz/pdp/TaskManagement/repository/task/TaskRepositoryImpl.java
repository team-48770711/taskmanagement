package uz.pdp.TaskManagement.repository.task;

import uz.pdp.TaskManagement.domain.DTO.Response;
import uz.pdp.TaskManagement.domain.model.Task;
import uz.pdp.TaskManagement.domain.model.User;
import uz.pdp.TaskManagement.repository.user.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.UUID;

public class TaskRepositoryImpl implements TaskRepository{
    private final ArrayList<Task> TASKS = new ArrayList<>();

    private static final TaskRepositoryImpl instance = new TaskRepositoryImpl();

    private TaskRepositoryImpl() {
    }

    public static TaskRepositoryImpl getInstance() {
        return instance;
    }
    @Override
    public Response save(Task task) {
        TASKS.add(task);
        return new Response<>("Success");
    }

    @Override
    public Task getById(UUID id) {
        return null;
    }

    @Override
    public ArrayList<Task> getAll() {
        return TASKS;
    }

    @Override
    public Response remove(Task task) {
        return null;
    }

    @Override
    public Response removeById(UUID id) {
        return null;
    }
}
