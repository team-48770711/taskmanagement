package uz.pdp.TaskManagement.controller;

import uz.pdp.TaskManagement.domain.model.Task;
import uz.pdp.TaskManagement.domain.model.TaskStatus;
import uz.pdp.TaskManagement.domain.model.User;

import java.util.ArrayList;

import static uz.pdp.TaskManagement.util.BeanUtil.scanNum;
import static uz.pdp.TaskManagement.util.BeanUtil.taskService;

public class Back_Front_DevUI {
    public static void BackFrontMenu(User user) {
        System.out.println();
        System.out.println("B_A_C_K     F_R_O_N_T    D_E_V   ");
        System.out.println("                           // Front *** ");
        System.out.println();
        System.out.println();
        System.out.println("1 - Do Task \t 2 - See Tasks \t 0 Exit ");
        System.out.print("Select tasks -> ");
        int select = scanNum.nextInt();
        switch (select) {
            case 1:
                doTask(user);
                break;
            case 2:
                see_Tasks(user);
                break;
            case 0:
                return;
            default:
                System.out.println("You entered an error in the selection, such a task does not exist ");
        }


    }

    private static void see_Tasks(User user) {
        taskService.printTasksByUserId(user.getId());
    }

    private static void doTask(User user) {
        taskService.printTasksByUserId(user.getId());

        System.out.print("Select the task you want to perform: ");
        int selectedTaskIndex = scanNum.nextInt();

        ArrayList<Task> userTasks = taskService.getTasksByUserId(user.getId());
        Task task = userTasks.get(selectedTaskIndex - 1);

        task.setTaskStatus(TaskStatus.DONE);

        System.out.println("Tast completed!!!");

        // todo testga o'tkazib yuborish kerak
    }


}
